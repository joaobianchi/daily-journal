const express		= require('express')
const bodyParser	= require('body-parser')
const ejs 			= require('ejs')
const _ 			= require('lodash')

const homeStartingContent	= 'Mussum Ipsum, cacilds vidis litro abertis. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Casamentiss faiz malandris se pirulitá. Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl. Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis.'
const aboutContent			= 'Mussum Ipsum, cacilds vidis litro abertis. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Casamentiss faiz malandris se pirulitá. Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl. Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis.'
const contactContent		= 'Mussum Ipsum, cacilds vidis litro abertis. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Casamentiss faiz malandris se pirulitá. Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl. Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis.'

const app = express()

app.set('view engine', 'ejs')

app.use(bodyParser.urlencoded({extended: true}))
app.use(express.static('public'))

let posts = []

app.get('/', function(req, res){
	res.render('home', {
		startingContent: homeStartingContent,
		posts: posts
	})
})

app.get('/about', function(req, res){
	res.render('about', {aboutStartingContent: aboutContent})
})

app.get('/contact', function(req, res){
	res.render('contact', {contactStartingContent: contactContent})
})

app.get('/compose', function(req, res){
	res.render('compose')
})

app.get('/post', function(req, res){
	res.render('post')
})

app.post('/compose', function(req, res){
	const post = {
		title: req.body.postTitle,
		content: req.body.postContent
	}

	posts.push(post)

	res.redirect('/')
})

app.get('/post/:postName', function(req, res){
	const requestedTitle = _.lowerCase(req.params.postName)

	posts.forEach(function(post){
		const storedTitle = _.lowerCase(post.title)

		if(storedTitle === requestedTitle){
			res.render('post', {
				title: post.title,
				content: post.content
			})
		}
	})
})

app.listen(3000, function(){
	console.log('Server started on port 3000')
})